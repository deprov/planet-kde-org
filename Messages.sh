#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-3.0-or-later

python3 translations.py extract $podir

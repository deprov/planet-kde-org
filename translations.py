# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-2.0-or-later

import os
import subprocess
import argparse
import gettext
import copy
import polib

from yaml import safe_load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


package = "planet-kde-org"


def extract(args):
    """
    Parameters:
    - pot: path of the directory containing the pot file we have to create
    """
    pot = polib.POFile(check_for_duplicates=True)
    pot.metadata = {
        'Project-Id-Version': f'{package} 1.0',
        'Report-Msgid-Bugs-To': 'https://bugs.kde.org',
        'Last-Translator': 'FULL NAME <EMAIL@ADDRESS>',
        'Language-Team': 'LANGUAGE <kde-i18n-doc@kde.org>',
        'MIME-Version': '1.0',
        'Content-Type': 'text/plain; charset=utf-8',
        'Content-Transfer-Encoding': '8bit',
    }

    entry = polib.POEntry(
        msgid=u'LANGUAGE_NAME',
        msgstr=u'',
        comment=u'Name of your language in your language (e.g. Français, Deutsch, ...)',
        occurrences=[('translations.py', '38')]
    )
    try:
        pot.append(entry)
    except:
        pass

    with open("i18n/en.yaml", 'r') as stream:
        en_string_trans = safe_load(stream)

        for key, string in en_string_trans.items():
            entry = polib.POEntry(
                msgid=string['other'],
                msgstr=u'',
                occurrences=[('i18n/en.yaml', '0')]
            )
            try:
                pot.append(entry)
            except:
                pass

    with open("config.yaml", "r") as stream:
        config = safe_load(stream)

        for menu_item in config['languages']['en']['menu']['main']:
            entry = polib.POEntry(
                msgid=menu_item['name'],
                msgstr=u'',
                occurrences=[('config.yaml', '0')]
            )
            try:
                pot.append(entry)
            except:
                pass

        entry = polib.POEntry(
            msgid=config['languages']['en']['title'],
            msgstr=u'',
            occurrences=[('config.yaml', '0')]
        )
        try:
            pot.append(entry)
        except:
            pass

        entry = polib.POEntry(
            msgid=config['languages']['en']['params']['description'],
            msgstr=u'',
            occurrences=[('config.yaml', '0')]
        )
        try:
            pot.append(entry)
        except:
            pass

    os.makedirs(args.pot, exist_ok=True)
    pot.save(f'{args.pot}/{package}.pot')


def import_po(args):
    """
    Parameters:
    - directory: path of the directory containing .po files in sub-paths {language}/{package}.po
    """
    directory = args.directory

    for language in os.listdir(directory):
        target_path = f"locale/{language}/LC_MESSAGES"
        os.makedirs(target_path)
        src = f"{directory}/{language}/{package}.po"

        command = f"msgfmt {src} -o {target_path}/{package}.mo"
        subprocess.run(command, shell=True, check=True)
        print(f"Translations for {language} imported")


lang_code_at_dict = {
    'cy' : 'cyrillic',
    'ije': 'ijekavian',
    'il' : 'ijekavianlatin',
    'la' : 'latin',
    'va' : 'valencia'
}

def revert_lang_code(hugo_lang_code):
    if hugo_lang_code == 'pt-pt':
        return 'pt'
    else:
        parts = hugo_lang_code.split('-')
        if len(parts) > 1:
            if parts[1] in lang_code_at_dict.keys():
                return f'{parts[0]}@{lang_code_at_dict[parts[1]]}'
            else:
                return f'{parts[0]}_{parts[1].upper()}'
        else:
            return hugo_lang_code


def generate_translations(args):
    """
    Assume translation located at `locale/$LANG/LC_MESSAGES/`
    """
    if not (os.path.isdir('content') and len(os.listdir('content')) > 0):
        return

    with open("i18n/en.yaml", 'r') as stream:
        en_string_trans = safe_load(stream)
    with open("config.yaml", 'r') as config_file:
        config_content = safe_load(config_file)

    content_lang_codes = os.listdir('content')
    lang_config = list(config_content['languages'].keys())
    for config_lang_code in lang_config:
        if not config_lang_code in content_lang_codes:
            del config_content['languages'][config_lang_code]

    for hugo_lang_code in content_lang_codes:
        if hugo_lang_code in ['en']:
            continue
        else:
            lang_code = revert_lang_code(hugo_lang_code)
            os.environ["LANGUAGE"] = lang_code
            gettext.bindtextdomain(package, 'locale')
            gettext.textdomain(package)
            _ = gettext.gettext

            trans_content = {}
            for key, string in en_string_trans.items():
                if _(string['other']) is not string['other']:
                    trans_content[key] = {}
                    trans_content[key]['other'] = _(string['other'])
            if len(trans_content) > 0:
                with open('i18n/' + hugo_lang_code + '.yaml', 'w+') as trans_file:
                    trans_file.write(dump(trans_content, default_flow_style=False, allow_unicode=True))

            if not hugo_lang_code in config_content['languages']:
                config_content['languages'][hugo_lang_code] = {}
            config_content['languages'][hugo_lang_code]['menu'] = {}
            config_content['languages'][hugo_lang_code]['menu']['main'] = list()
            config_content['languages'][hugo_lang_code]['languageCode'] = hugo_lang_code
            config_content['languages'][hugo_lang_code]['params'] = dict()
            config_content['languages'][hugo_lang_code]['params']['description'] = _(config_content['languages']['en']['params']['description'])
            config_content['languages'][hugo_lang_code]['title'] = _(config_content['languages']['en']['title'])
            config_content['languages'][hugo_lang_code]['weight'] = 2

            tr = _("LANGUAGE_NAME")
            if tr != "LANGUAGE_NAME":
                config_content['languages'][hugo_lang_code]['languageName'] = tr

            for menu_item in config_content['languages']['en']['menu']['main']:
                menu = copy.deepcopy(menu_item)
                menu['name'] = _(menu['name'])
                config_content['languages'][hugo_lang_code]['menu']['main'].append(menu)

    with open('config.yaml', 'w+') as conf_file:
        conf_file.write(dump(config_content, default_flow_style=False, allow_unicode=True))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='sub-command help')

    extract_cmd = subparsers.add_parser('extract', help='extract strings for translations')
    extract_cmd.add_argument('pot')
    extract_cmd.set_defaults(func=extract)

    import_po_cmd = subparsers.add_parser('import', help='import translated strings')
    import_po_cmd.add_argument('directory')
    import_po_cmd.set_defaults(func=import_po)

    generate_translations_cmd = subparsers.add_parser('generate-translations', help='generate translated content')
    generate_translations_cmd.set_defaults(func=generate_translations)

    args = parser.parse_args()
    args.func(args)

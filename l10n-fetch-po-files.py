#!/usr/bin/python

# SPDX-FileCopyrightText: 2021 Nguyen Hung Phu <phuhnguyen@disroot.org>
# SPDX-License-Identifier: LGPL-3.0-or-later

""" Fetch .po files from KDE SVN
"""

import os
import subprocess

SVN_PRE_PATH = "svn://anonsvn.kde.org/home/kde/trunk/l10n-kf5"
SVN_SUB_PATH = "messages/websites-planet-kde-org"
PO_PATH = "pos"

if not os.path.exists(PO_PATH):
    os.mkdir(PO_PATH)

# all_languages = "af ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia crh cs csb cy da de el en_GB eo es et eu fa fi fr fy ga gd gl gu ha he hl hne hr hsb hu hy ia id is it ja ka kk km kn ko ku lb lt lv mai mk ml mr ms mt nb nds ne nl nn nso oc or pa pl ps pt pt_BR ro ru rw se si sk sl sq sr sr@ijekavian sr@ijekavianlatin sr@latin sv ta te tg th tn tr tt ug uk uz uz@cyrillic vi wa xh zh_CN zh_HK zh_TW".split(" ")
all_languages = "ca es eu fr it nl pt ru sk sv tr uk zh_CN".split(" ")

for lang in all_languages:
    SVN_PATH = f"{SVN_PRE_PATH}/{lang}/{SVN_SUB_PATH}"
    OUTPUT = f"{PO_PATH}/{lang}"
    try:
        subprocess.check_output(['svn', 'export', SVN_PATH, OUTPUT], stderr=subprocess.PIPE)
        print(f"Fetched {lang}")
    except subprocess.CalledProcessError:
        print(f"Non-existent {lang}")
